import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import SeatList from "./SeatList";
import "./style.css";

class MovieSeatBooking extends Component {
  render() {
    return (
      <div className="d-flex ">
        <div className="mx-5">
          <div className="movie-container">
            <label className="text-light">Pick a movie:</label>
            <select id="movie">
              <option value="{10}">Avenges : Endgame</option>
            </select>
          </div>

          <ul className="showcase">
            <li>
              <div className="seat" />
              <small>N/A</small>
            </li>
            <li>
              <div className="seat selected" />
              <small>Selected</small>
            </li>
            <li>
              <div className="seat occupied" />
              <small>Occupied</small>
            </li>
          </ul>
          <SeatList data={this.props.data} />
        </div>
        <div className="mx-5 pt-5 ">
          <Cart />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.MovieReducer.data,
    quantity: state.MovieReducer.seatsSelect.length,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieSeatBooking);
