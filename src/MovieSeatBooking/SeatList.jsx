import React, { Component } from "react";
import { connect } from "react-redux";
import SeatRow from "./SeatRow";

class SeatList extends Component {
  render() {
    return (
      <div className="container">
        <div className="screen"></div>
        {this.props.data.map((e, i) => {
          return <SeatRow key={i} dataRow={e} />;
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // data: state.MovieReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatList);
