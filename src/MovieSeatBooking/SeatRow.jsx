import React, { Component } from "react";
import { connect } from "react-redux";
import SeatItem from "./SeatItem";

class SeatRow extends Component {
  render() {
    const isSelect = (soGhe) => {
      if (this.props.seatsSelect.length !== 0) {
        let index = this.props.seatsSelect.findIndex((e, i) => {
          return e.soGhe === soGhe;
        });
        return index !== -1;
      }
      return false;
    };

    return (
      <div className="row">
        {this.props.dataRow.danhSachGhe.map((e, i) => {
          return <SeatItem data={e} key={i} isSelect={isSelect(e.soGhe)} />;
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    seatsSelect: state.MovieReducer.seatsSelect,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatRow);
