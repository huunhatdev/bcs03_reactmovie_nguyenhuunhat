import data from "../../data.json";
import { CHON_GHE, HUY_CHON_GHE, THANH_TOAN } from "../constants/constants";

const initialState = {
  data: data,
  seatsSelect: [],
};

export default (state = initialState, { type, payload }) => {
  let cloneS = { ...state };
  let json = null;

  const findSeat = (soGhe) => {
    for (let i = 0; i < cloneS.data.length; i++) {
      for (let ii = 0; ii < cloneS.data[i].danhSachGhe.length; ii++) {
        if (cloneS.data[i].danhSachGhe[ii].soGhe === soGhe) {
          return { row: i, index: ii };
        }
      }
    }
  };

  switch (type) {
    case CHON_GHE:
      let seatInfo = payload;
      cloneS.seatsSelect.push(seatInfo);
      json = JSON.stringify(cloneS);
      return { ...JSON.parse(json) };

    case HUY_CHON_GHE:
      let index = cloneS.seatsSelect.findIndex((e, i) => {
        return e.soGhe === payload;
      });
      if (index !== -1) {
        cloneS.seatsSelect.splice(index, 1);
        json = JSON.stringify(cloneS);
      }
      return { ...JSON.parse(json) };

    case THANH_TOAN:
      cloneS.seatsSelect.forEach((e, i) => {
        let payload = findSeat(e.soGhe);
        cloneS.data[payload.row].danhSachGhe[payload.index].daDat = true;
      });
      cloneS.seatsSelect = [];
      json = JSON.stringify(cloneS);
      return { ...JSON.parse(json) };

    default:
      return state;
  }
};
