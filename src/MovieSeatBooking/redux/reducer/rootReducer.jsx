import { combineReducers } from "redux";
import MovieReducer from "./movieReducer";

export const rootMovie = combineReducers({ MovieReducer });
