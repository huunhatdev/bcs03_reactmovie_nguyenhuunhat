import { createStore } from "redux";
import { rootMovie } from "./reducer/rootReducer";

export const StoreMovie = createStore(
  rootMovie,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
