import React, { Component } from "react";
import { connect } from "react-redux";

class SeatItem extends Component {
  renderSeat = () => {
    let item = this.props;
    if (item.data?.daDat) {
      return <div className="seat occupied " />;
    }
    if (item.isSelect) {
      return (
        <div
          className="seat selected"
          onClick={() => this.props.handleUnSelect(item.data.soGhe)}
        />
      );
    }
    return (
      <div
        className="seat"
        onClick={() => this.props.handleSelect(item.data)}
      />
    );
  };
  render() {
    return this.renderSeat();
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSelect: (data) => {
      dispatch({
        type: "CHON_GHE",
        payload: data,
      });
    },
    handleUnSelect: (seat) => {
      dispatch({
        type: "HUY_CHON_GHE",
        payload: seat,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatItem);
