import React, { Component } from "react";
import { connect } from "react-redux";
import { HUY_CHON_GHE, THANH_TOAN } from "./redux/constants/constants";

class Cart extends Component {
  render() {
    const renderTable = () => {
      return this.props.dataMovie.map((e, i) => {
        return (
          <tr key={i}>
            <th scope="row">{i + 1}</th>
            <td>{e.soGhe}</td>
            <td>{e.gia}</td>
            <td>
              <i
                className="bi bi-trash3"
                onClick={() => this.props.handleUnSelect(e.soGhe)}
              ></i>
            </td>
          </tr>
        );
      });
    };

    const price = () => {
      if (this.props.dataMovie.length !== 0) {
        let sum = 0;
        this.props.dataMovie.forEach((e, i) => {
          sum += e.gia;
        });
        return (sum * 1).toLocaleString("vi", {
          style: "currency",
          currency: "VND",
        });
      }
      return "0đ";
    };

    return (
      <>
        <h4 className=" text-white">Selected tickets</h4>
        <table className="table text-white">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Seat</th>
              <th scope="col">Price</th>
              <th scope="col">Del</th>
            </tr>
          </thead>
          <tbody>{renderTable()}</tbody>
        </table>
        <button
          className="btn btn-secondary"
          onClick={() => this.props.handlePay()}
        >
          Payment ({price()})
        </button>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataMovie: state.MovieReducer.seatsSelect,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleUnSelect: (seat) => {
      dispatch({
        type: HUY_CHON_GHE,
        payload: seat,
      });
    },
    handlePay: () => {
      dispatch({
        type: THANH_TOAN,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
